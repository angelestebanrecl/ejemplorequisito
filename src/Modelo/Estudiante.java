/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 * Requisitos:
 *      1. Un objeto es igual a otro si tienen el mismo valor para todos sus parciales.
 *      2. Un objeto es >,< con el análisis del promedio de sus notas según nuestro reglamento
 * 
 * @author madar
 */
public class Estudiante implements Comparable{
    
    private long codigo;
    private float p1, p2,p3, examen;
    private float promedio;
    
       
    //Requisito 1:

    public Estudiante() {
    }

    public Estudiante(long codigo, float p1, float p2, float p3, float promedio) {
        this.codigo = codigo;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.promedio = promedio;
    }
    
    //Requisito 2: 

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public float getP1() {
        return p1;
    }

    public void setP1(float p1) {
        this.p1 = p1;
    }

    public float getP2() {
        return p2;
    }

    public void setP2(float p2) {
        this.p2 = p2;
    }

    public float getP3() {
        return p3;
    }

    public void setP3(float p3) {
        this.p3 = p3;
    }

    public float getExamen() {
        return examen;
    }

    public void setExamen(float examen) {
        this.examen = examen;
    }

    public float getPromedio() {
        return promedio;
    }

    public void setPromedio(float promedio) {
        this.promedio = promedio;
    }
    
    //Requisito 3:

    @Override
    public String toString() {
        return "Estudiante{" + "codigo=" + codigo + ", p1=" + p1 + ", p2=" + p2 + ", p3=" + p3 + ", examen=" + examen + '}';
    }

        //requisito 4:

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Float.floatToIntBits(this.p1);
        hash = 19 * hash + Float.floatToIntBits(this.p2);
        hash = 19 * hash + Float.floatToIntBits(this.p3);
        hash = 19 * hash + Float.floatToIntBits(this.examen);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estudiante other = (Estudiante) obj;
        if (Float.floatToIntBits(this.p1) != Float.floatToIntBits(other.p1)) {
            return false;
        }
        if (Float.floatToIntBits(this.p2) != Float.floatToIntBits(other.p2)) {
            return false;
        }
        if (Float.floatToIntBits(this.p3) != Float.floatToIntBits(other.p3)) {
            return false;
        }
        if (Float.floatToIntBits(this.examen) != Float.floatToIntBits(other.examen)) {
            return false;
        }
        return true;
    }
    
    //requisito 5:

    @Override
    public int compareTo(Object obj) {
        if (this == obj) {
            return 0;
        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
        final Estudiante other = (Estudiante) obj;
        
        if(this.promedio==other.getPromedio())
            return 0;
        
        if(this.promedio>other.getPromedio())
            return 1;
        
        if(this.promedio<other.getPromedio())
            return -1;
        
        return 0;
    }
    
    public float calcularPromedio(){
        
        promedio = (p1+p2+p3)/3;
        
        return promedio;
        
    
    }
}
