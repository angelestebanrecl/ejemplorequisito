/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;

/**
 *  SU TAREA CONSISTE EN IMPLEMENTAR LOS REQUISITOS DE UNA CLASE DE OBJETO DE TIPO ESTUDIANTE
 * DEBE PROBARLO CON 5 OBJETOS DIFERENTES Y REALIZAR COMPARACIONES ENTRE ELLOS
 * @author madar
 */
public class PruebaEstudiante {
    public static void main(String[] args){
        Estudiante a=new Estudiante(1, (float) 3.5, (float) 4.5, 4, (float) 3.8);
        Estudiante b=new Estudiante(2, (float) 3.5, (float) 3.5, (float) 3.5, (float) 3.5);
        Estudiante c=new Estudiante(3, (float) 3.5, (float) 4.1, (float) 4.5, (float) 3.8);
        Estudiante d=new Estudiante(4, (float) 5.0, (float) 5.0, (float) 5.0, (float) 5.0);
        Estudiante e=new Estudiante(5, (float) 3.5, (float) 3.5, (float) 3.5, (float) 3.5);
        
        //imprimir los objetos:
        System.out.println("A:"+a.toString());
        System.out.println("B:"+b.toString());
        System.out.println("C:"+c.toString());
        System.out.println("D:"+d.toString());
        System.out.println("E:"+e.toString());
        
        //comparando si son iguales por sus promedios:
        
        if(b.equals(e))
            System.out.println("si son iguales por su promedio");
        else
            System.out.println("no son iguales por su promedio");
        
        //comparar:
        
        int comparador=b.compareTo(e);
        System.out.println("valor del comparador:"+comparador);
        
    
    }
            
}
